<?php
include_once "vendor/autoload.php";

use Pondit\Library\Book;
use Pondit\Library\Author;
use Pondit\Library\Publisher;


$book = new Book();
echo $book->book('Bangla');
echo '<br/>';


$author = new Author();
echo $author->author('Mahfuz');
echo '<br/>';


$publisher = new Publisher();
echo $publisher->publisher('Pondit Publication');
